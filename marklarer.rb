class Marklarer
  @@max_word_length = 4
  @@replace_word = 'marklar'
  @@delimiter = ' '

  # Start Marklarer
  # @param [String] str
  # @return [String]
  def process(str)
    raise "Failed to process string" if str.empty?

    str_arr = str.split(@@delimiter)
    str_arr = str_arr.map do |element|
      # replace every word
      replace(element)
    end

    str_arr.join(@@delimiter)
  end

  # Replace every word that exceeds 4 characters with "marklar".
  # @param [String] str
  # @return [String]
  def replace(str)
    if (length(str) > @@max_word_length)

      new_string = @@replace_word
      if (has_punctuation?(str))
        new_string += str[-1,1]
      end

      if has_uppercase?(str)
        new_string = new_string.capitalize
      end

      return new_string
    else
      return str
    end
  end

  # Get string length
  # @param [String] str
  # @return [Integer]
  def length(str)
    if (has_punctuation?(str))
      return str.size - 1
    end

    return str.size
  end

  # Check if word has punctuation characters
  # @param [String] str
  # @return [Boolean]
  def has_punctuation?(str)
    last_char = str[-1,1]
    last_char =~ /[^a-z0-9]/
  end

  # Check if word has uppercase characters
  # @param [String] str
  # @return [Boolean]
  def has_uppercase? (str)
    str == str.capitalize
  end

  private :length, :replace, :has_uppercase?, :has_punctuation?
end