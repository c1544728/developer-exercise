class EvenFibonacci
  ZERO_NUMBER = 0
  FIRST_NUMBER = 1
  def self.calculate(num_elements)
    sum_result = 0
    start = 1

    prev_number = ZERO_NUMBER
    cur_number = FIRST_NUMBER

    begin
      temp = cur_number
      cur_number = cur_number + prev_number
      prev_number = temp
      if (cur_number.even?)
        sum_result += cur_number
      end
      start += 1
    end while start < num_elements

    return sum_result
  end
end